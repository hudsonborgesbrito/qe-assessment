User SignUp Story

Narrative:
In order to buy sporting goods
As a customer
I want to be able to SignUp to Netshoes web site
So that I can login to Netshoes web site

Scenario: The customer should be able to sign up to the web site
Given the user access the NetShoes home page
When the user click on the 'Login' button
And the user fill the field 'Informe seu e-mail' with the value '<email>' on the 'Criar Conta' box
And the user press the button 'Prosseguir'
Then the user should see the registry form
When the user fill the 'Pessoa Física' mandatory fields
And the user fill the 'Sobre sua conta' mandatory fields
And the user press the 'Continuar' button
Then the user should see the message 'Olá, <firstName>'

Examples:
|email|firstName|lastName|gender|day_birthDate|month_birthDate|year_birthDate|cpf|zipCode|addressNumber|district|state|city|phoneNumber|password|
|john@doe.com|John|Doe|Masculino|01|Apr|2000|617.448.602-89|83035-350|157|Vila Pinto|Paraná|Curitiba|(44) 54545-4545|123@456|
