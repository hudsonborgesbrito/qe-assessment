Shopping Cart Story
Narrative:
In order to buy sporting goods
As a customer
I want to be able to add and remove products to/from shopping cart
So that I can check my purchase price with the shipping cost for the amount of items in shopping cart

Lifecycle:
Scope: SCENARIO
Before:
Outcome: ANY
Given cleanup shopping cart if products are present

Scenario: The customer should be able to add items in the shopping cart
Given the user access the NetShoes home page
When the user searches for the product <productName>
Then the search results are displayed
When the user adds <amountOfProductsToAdd> products in the shopping cart
And the user clicks on the shopping cart icon
Then the shopping cart is displayed
And the shopping cart contains all products added by the user (<amountOfProductsToAdd>)
And the total price should be the sum of the individual prices of each item
Examples:
|productName|amountOfProductsToAdd|
|Nike|2|

Scenario: The customer should be able to calculate the shipping cost in the shopping cart
Given the user access the NetShoes home page
When the user searches for the product <productName>
Then the search results are displayed
When the user adds <amountOfProductsToAdd> products in the shopping cart
And the user clicks on the shopping cart icon
Then the shopping cart is displayed
When the user informs the CEP '<CEP>' in the 'Frete' field
And the user clicks on 'Calcular Frete'
Then the shipping cost '<FRETE_VALUE_EXPECTED>' should be displayed to the user
Examples:
|productName|amountOfProductsToAdd|CEP|FRETE_VALUE_EXPECTED|
|Nike|1|83035-350|FRETE GRÁTIS|


Scenario: The customer should be able to clear the shopping cart
Given the user access the NetShoes home page
When the user searches for the product <productName>
Then the search results are displayed
When the user adds <amountOfProductsToAdd> products in the shopping cart
And the user clicks on the shopping cart icon
Then the shopping cart is displayed
And the shopping cart contains all products added by the user (<amountOfProductsToAdd>)
When the user clicks on 'Limpar Carrinho'
Then all added items are removed from the shopping cart
Examples:
|productName|amountOfProductsToAdd|
|Nike|1|
