Product Search Story

Narrative:
In order to buy sporting goods
As a customer
I want to be able to access and search for products in the web store
So that I can search for Products using different search criterias


Scenario: The customer should be able to search for specific brands and products
Given the user access the NetShoes home page
When the user searches for the product <productName>
Then the search results are displayed
And only items corresponding to the search term '<productName>' are displayed
Examples:
|productName|
|Nike|

Scenario: The product search result page should display more than 5 items
Given the user access the NetShoes home page
When the user searches for the product <productName>
Then the search results are displayed
And more than 5 items should be displayed in the result
Examples:
|productName|
|Nike|