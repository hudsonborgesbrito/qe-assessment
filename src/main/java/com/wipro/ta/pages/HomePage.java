package com.wipro.ta.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;

import com.wipro.ta.configuration.PageObject;

@PageObject
public class HomePage extends AbstractPage {

	protected Logger LOG = Logger.getLogger(this.getClass());

	@Value("${home.url}")
	private String NETSHOES_HOMEPAGE_URL;

	@FindBy(className = "content")
	private WebElement contentDiv;

	@FindBy(id = "search-input")
	private WebElement searchTextInput;

	@FindBy(xpath = "//button[@qa-automation='home-search-button']")
	private WebElement searchButton;
	
	

	public void openPage() {
		LOG.info("Navigating user to page: " + NETSHOES_HOMEPAGE_URL);
		super.webDriverProvider.get().get(NETSHOES_HOMEPAGE_URL);
		// check if the floating advertising is shown
		try {
			WebElement element = super.webDriverProvider.get().findElement(
					By.xpath("//div[@id='bg-sombra-floater']/*/div[@class='div-close']/span[@class='fechar-x']"));
			if (element != null && element.isDisplayed()) {
				element.click();
			}
		} catch (Exception e) {
			LOG.warn("Advertising element not found");
		}
	}

	public void performSearch(String searchCriteria) {
		LOG.info("Searching for: " + searchCriteria);
		this.searchTextInput.sendKeys(searchCriteria);
		this.searchButton.click();
	}

	public boolean isContentDisplayed() {
		return this.contentDiv.isDisplayed();
	}
	
	public boolean hasItensInCart(){
		By cartCountBadgeLocator = By.xpath("//a[@qa-automation='home-cart-button']/span[@class='cart-count-badge']/span");
		
		WebDriverWait waiter = new WebDriverWait(super.webDriverProvider.get(), 10);
		waiter.until(ExpectedConditions.visibilityOfElementLocated(cartCountBadgeLocator));
		
		WebElement cartCountBadge = super.webDriverProvider.get().findElement(cartCountBadgeLocator);
		return !"0".equals(cartCountBadge.getText());
	}
	
	public void clickShoppingCartIcon() {
		String shoppingCartIconXpath="//a[@qa-automation='home-cart-button']";
		WebDriverWait waiter = new WebDriverWait(super.webDriverProvider.get(), 10);
		By byXPath = By.xpath(shoppingCartIconXpath);
		waiter.until(ExpectedConditions.and(
			ExpectedConditions.visibilityOfAllElementsLocatedBy(byXPath),
			ExpectedConditions.elementToBeClickable(byXPath)
		));

		WebElement shoppingCartIcon = super.webDriverProvider.get().findElement(byXPath);
		Actions action = new Actions(super.webDriverProvider.get());
		action.moveToElement(shoppingCartIcon).click().perform();
	}

}
