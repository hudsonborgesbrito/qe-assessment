package com.wipro.ta.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.codehaus.plexus.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wipro.ta.configuration.PageObject;

@PageObject
public class SearchResultPage extends AbstractPage {

	@FindBy(xpath = "//section[@class='search-list search-list--3']")
	private WebElement searchResultSection;

	@FindBy(xpath = "//section[@class='search-list search-list--3']/div[@id='item-list']/div[@class='wrapper']/div/div/a/span[@itemprop='name']")
	private List<WebElement> searchResultProductDescriptionList;

	@FindBy(xpath = "//div[@qa-automation='search-itens']")
	private List<WebElement> listProductsItems;

	public boolean isSearchResultsDisplayed() {
		return this.searchResultSection.isDisplayed();
	}

	public boolean areAllResultsShownRelatedToSearchCriteria(String searchCriteria) {
		if (searchResultProductDescriptionList == null || searchResultProductDescriptionList.isEmpty()) {
			return false;
		}

		for (WebElement spanElement : searchResultProductDescriptionList) {
			if (!spanElement.getText().toLowerCase().contains(searchCriteria.toLowerCase())) {
				return false;
			}
		}

		return true;
	}

	public boolean isShowingMoreThanElements(int numberOfElements) {
		return searchResultProductDescriptionList != null
				&& searchResultProductDescriptionList.size() > numberOfElements;
	}

	public boolean addItemToCart(int itemPosition) {
		WebDriverWait waiter = new WebDriverWait(super.webDriverProvider.get(), 30);

		// select product Item from search result to open product detail modal
		if (!this.selectProductItem(itemPosition, waiter)) {
			return false;
		}
		LOG.info("Product is selected, waiting product details modal to be displayed.");

		//wait modal to be loaded
		By byLocatorModalProductDetail = By.xpath("//div[@id='quickview-container']/div");
		waiter.until(ExpectedConditions.visibilityOfElementLocated(byLocatorModalProductDetail));
		LOG.info("Modal product details is shown. Performing select item size");

		// select Item size inside product detail modal
		String dataItemSkuSize = this.selectItemSize();
		if (StringUtils.isEmpty(dataItemSkuSize)) {
			return false;
		}
		
		LOG.info("Item Size is selected, waiting to refresh fields inside product detail");
		//mudar para o dataloading animate
		waiter.until(ExpectedConditions.and(
			ExpectedConditions.attributeToBe(By.xpath("//section[@class='product-size-selector']/div/ul"), "data-loading-animate", ""),
			ExpectedConditions.invisibilityOfElementLocated(By.xpath("//section[@class='product-size-selector']/div/ul[@data-loading-animate='active'")),
			ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-sku-changed-target]/section[@class='showcase three-col']"))
		));

		LOG.info("Size is selected, fields are refreshed inside product detail, performing add button");
		// press btn ADD inside product detail modal
		this.clickAddButton(waiter, dataItemSkuSize);

		LOG.info("Add button is performed, Wait for modal product detail to be hidden");
		// wait the cart summary to hide
		waiter.until(ExpectedConditions.invisibilityOfElementLocated(byLocatorModalProductDetail));

		LOG.info("Div modal product detail is hidden, moving forward");

		return true;
	}

	private void clickAddButton(WebDriverWait waiter, String dataSkuItemSize) {
		String xpathBtnAdd = "//section[@id='buy-box']/section[@id='action-buttons']/div[@id='buy-button-wrapper']/button[@qa-automation='product-buy-button']";

		waiter.pollingEvery(100, TimeUnit.MILLISECONDS).until(ExpectedConditions.and(
			ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpathBtnAdd)),
			ExpectedConditions.attributeContains(By.xpath(xpathBtnAdd), "data-sku", dataSkuItemSize)
		));

		WebElement btnAddItem = super.webDriverProvider.get().findElement(By.xpath(xpathBtnAdd));
		waiter.until(ExpectedConditions.elementToBeClickable(btnAddItem));
		btnAddItem.click();
	}

	private boolean selectProductItem(int itemPosition, WebDriverWait waiter) {

		waiter.until(ExpectedConditions.visibilityOfAllElements(this.listProductsItems));

		WebElement divItemSearchResult = this.listProductsItems.get(itemPosition);

		if (divItemSearchResult == null) {
			return false;
		}

		waiter.until(ExpectedConditions.and(ExpectedConditions.visibilityOf(divItemSearchResult),
				ExpectedConditions.elementToBeClickable(divItemSearchResult)));

		Actions action = new Actions(super.webDriverProvider.get());
		action.moveToElement(divItemSearchResult).perform();

		WebElement btnConferir = divItemSearchResult.findElement(By.xpath("./a/button[@id='quick-view-button']"));

		waiter.until(ExpectedConditions.and(ExpectedConditions.visibilityOf(btnConferir),
				ExpectedConditions.elementToBeClickable(btnConferir)));

		btnConferir.click();
		return true;
	}

	private String selectItemSize() {
		List<WebElement> sizesAvailableList = super.webDriverProvider.get().findElements(
				By.xpath("//section[@class='product-size-selector']/div/ul/li[*]/a[@qa-option!='unavailable']"));

		if (sizesAvailableList == null || sizesAvailableList.isEmpty()) {
			return null;
		}
		// select available Size
		for (int i = sizesAvailableList.size() - 1; i >= 0; i--) {
			WebElement sizeItem = sizesAvailableList.get(i);
			if (sizeItem.isEnabled()) {
				String dataSkuItemSize = sizeItem.getAttribute("data-sku");
				sizeItem.click();
				return dataSkuItemSize;
			}
		}
		return null;
	}

}
