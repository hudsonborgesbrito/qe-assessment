package com.wipro.ta.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wipro.ta.configuration.PageObject;

@PageObject
public class ShoppingCartPage extends AbstractPage {

	@FindBy(xpath="//p[@qa-automation='cart-product-price']")
	private List<WebElement> listItemPrices;

	@FindBy(xpath="//p[@qa-automation='cart-price']")
	private WebElement totalShoppingCartItemsValue;

	@FindBy(xpath="//input[@qa-automation='cart-cep-field']")
	private WebElement freteInputField;

	@FindBy(xpath="//button[@qa-automation='cart-cep-button']")
	private WebElement btnCalcularFrete;

	@FindBy(xpath="//a[@qa-automation='cart-erase-cart-button']")
	private WebElement limparCarrinhoAnchor;


	public boolean isShoppingCartDisplayed(){
		return totalShoppingCartItemsValue == null ? false : totalShoppingCartItemsValue.isDisplayed();
	}

	public boolean isItemListSizeEqualNumberOfItemsAdded(int numberOfItemsAdded){
		return listItemPrices == null ? false : listItemPrices.size()== numberOfItemsAdded;
	}

	public double getSumTotalItemsCart(){
		if(listItemPrices == null || listItemPrices.isEmpty()){
			return -1;
		}
		Double total = Double.valueOf(0);
		for(WebElement productPriceWebElement : listItemPrices){
			total += this.convertPriceToDouble(productPriceWebElement.getText());
			LOG.info("partial sum:"+total);
		}
		return total;
	}

	public double getTotalShoppingCart(){
		if(totalShoppingCartItemsValue == null || StringUtils.isEmpty(totalShoppingCartItemsValue.getText())){
			return -1;
		}

		return Double.valueOf(this.convertPriceToDouble(totalShoppingCartItemsValue.getText()));
	}

	public Double convertPriceToDouble(String price){
		String value = price.replaceAll("[^-?0-9,]", "");
		value = value.replaceAll(",",".");
		return Double.valueOf(value);
	}

	public void fillFreteField(String value){
		LOG.info("Will type a value on field 'Frete'");
		freteInputField.sendKeys(value);
		LOG.info("Value informed on field 'Frete'");
	}

	public void pressCalcularFreteButton(){
		LOG.info("Will perform a click on button 'Calcular Frete'");
		btnCalcularFrete.click();
		LOG.info("Performed a click on button 'Calcular Frete'");
	}

	public String getValorFrete(){
		String xpathFreteText="//p[@class='column-value-shipping']/strong[@class='cart-free-shipping-text']";
		LOG.info("Waiting shipping value to be shown");
		WebDriverWait waiter = new WebDriverWait(super.webDriverProvider.get(), 5);
		waiter.pollingEvery(200, TimeUnit.MILLISECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathFreteText)));

		WebElement valorFreteGratisStrong = super.webDriverProvider.get().findElement(By.xpath(xpathFreteText));
		if(valorFreteGratisStrong.isDisplayed()){
			LOG.info("Shipping value: "+ valorFreteGratisStrong.getText());
			return valorFreteGratisStrong.getText();
		}
		LOG.info("Shipping value not displayed");
		return null;
	}

	public void pressLimparCarrinhoAnchor(){
		LOG.info("will click on Limpar Carrinho anchor");
		limparCarrinhoAnchor.click();
		LOG.info("clicked on Limpar Carrinho anchor");
	}

}
