package com.wipro.ta.steps;

import org.apache.log4j.Logger;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.ta.pages.HomePage;
import com.wipro.ta.pages.ShoppingCartPage;

@Component
public class NetshoesShoppingCartPageSteps extends AbstractSteps {

	protected Logger LOG = Logger.getLogger(this.getClass());

	@Autowired
	private ShoppingCartPage shoppingCartPage;

	@Autowired
    protected HomePage homePage;

	@Then("the shopping cart is displayed")
	public void thenTheShoppingCartIsDisplayed(){
		Assert.assertTrue("The shopping cart is not displayed", shoppingCartPage.isShoppingCartDisplayed());
	}

	@Then("the shopping cart contains all products added by the user ($numberOfItemsAdded)")
	public void thenTheShoppingCartContainsAllProductsAddedByTheUser(@Named("numberOfItemsAdded") int numberOfItemsAdded){
		Assert.assertTrue("The shopping cart does not contain the same number of items it was added", shoppingCartPage.isItemListSizeEqualNumberOfItemsAdded(numberOfItemsAdded));
	}
	@Then("the total price should be the sum of the individual prices of each item")
	public void thenTheTotalPriceShouldBeTheSumOfAllItems(){
		Assert.assertEquals(shoppingCartPage.getSumTotalItemsCart(), shoppingCartPage.getTotalShoppingCart(), 0);
	}

	@When("the user informs the CEP '$CEP' in the 'Frete' field")
	public void whenTheUserInformsTheCepInFreteField(@Named("CEP") String cep){
		shoppingCartPage.fillFreteField(cep);
	}

	@When("the user clicks on 'Calcular Frete'")
	public void whenTheUserClicksOnCalcularFrete(){
		shoppingCartPage.pressCalcularFreteButton();
	}

	@Then("the shipping cost '$FRETE_VALUE_EXPECTED' should be displayed to the user")
	public void thenTheShippingCostShouldAppearAsFreteGratis(@Named("FRETE_VALUE_EXPECTED") String freteValue){
		Assert.assertEquals(freteValue,shoppingCartPage.getValorFrete());
	}

	@When("the user clicks on 'Limpar Carrinho'")
	public void whenTheUserClicksOnLimparCarrinho(){
		shoppingCartPage.pressLimparCarrinhoAnchor();
	}

	@Then("all added items are removed from the shopping cart")
	public void thenAllAddedItemsAreRemovedFromTheShoppingCart(){
		Assert.assertTrue("Shopping cart was supposed to be empty but it has some items",homePage.hasItensInCart());

	}

	@Given("cleanup shopping cart if products are present")
	public void thenCleanupShoppingCartIfProductsArePresent(){
		LOG.info("cleanup shopping cart if products are present");

		LOG.info("Openning Home page");
		homePage.openPage();

		if(homePage.hasItensInCart()){
			LOG.info("Items found in Cart");
			LOG.info("Click shopping cart icon");
			homePage.clickShoppingCartIcon();
			LOG.info("Press 'Limpar Carrinho' anchor");
			shoppingCartPage.pressLimparCarrinhoAnchor();
		}else{
			LOG.info("No items present in cart");
		}
	}
}
