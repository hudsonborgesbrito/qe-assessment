package com.wipro.ta.steps;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.ta.pages.HomePage;

@Component
public class NetshoesHomePageSteps extends AbstractSteps {

    @Autowired
    protected HomePage homePage;

    @Given("the customer access the NetShoes home page")
    @Alias("the user access the NetShoes home page")
    public void givenCustomerAccessHomePage() {
    	homePage.openPage();
    }

    @When("the user searches for the product $productName")
    public void whenTheUserSearchForAProduct(@Named("productName") String productName){
    	homePage.performSearch(productName);
    }

    @When("the user clicks on the shopping cart icon")
	public void whenTheUserClickOnTheShoppingCartIcon(){
		homePage.clickShoppingCartIcon();
	}

    @Then("the products content should be displayed")
    public void thenProductListIsDisplayed() {
    	Assert.assertTrue("The available products list was expected to be displayed, but it was not.", homePage.isContentDisplayed());
    }
}