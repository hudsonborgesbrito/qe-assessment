package com.wipro.ta.steps;

import org.apache.log4j.Logger;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.ta.pages.SearchResultPage;

@Component
public class NetshoesSearchResultPageSteps extends AbstractSteps {

	protected Logger LOG = Logger.getLogger(this.getClass());

	@Autowired
	private SearchResultPage searchResultPage;

	@When("the user adds $amountOfProductsToAdd products in the shopping cart")
	public void whenTheUserAddProductsToShoppingCart(@Named("amountOfProductsToAdd") Integer numberOfProducts) {
		LOG.info("starting to add " + numberOfProducts + " products");
		for (int i = 0; i < numberOfProducts; i++) {
			Assert.assertTrue("Product was supposed to be added to cart, but was not",searchResultPage.addItemToCart(i));
		}
	}

	@Then("the search results are displayed")
	public void thenTheSearchResultsAreDisplayed() {
		Assert.assertTrue("The search results was expected to be displayed, but it was not.",
				searchResultPage.isSearchResultsDisplayed());
	}

	@Then("only items corresponding to the search term '$productName' are displayed")
	public void thenOnlyItemsCorrespondingToSearchTermAreDisplayed(@Named("productName") String productName) {
		Assert.assertTrue("Only products related to the search criteria should be displayed, but it was not.",
				searchResultPage.areAllResultsShownRelatedToSearchCriteria(productName));
	}

	@Then("more than 5 items should be displayed in the result")
	public void thenMoreThan5ItemsShouldBeDisplayed() {
		Assert.assertTrue("More than 5 results should be displayed, but it was not.",
				searchResultPage.isShowingMoreThanElements(5));
	}

}
